import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    estudiantes: [],
    municipios: []
  },
  mutations: {
    setEstudiantes(state, payload){
      state.estudiantes = payload;
    },
    pushEstudiante(state, obj){
      state.estudiantes.push(obj);
    },
    setMunicipios(state, payload){
      state.municipios = payload;
    }
  },
  actions: {
    async cargarEstudiantes({commit}){
      //GET
      const peticion = await fetch('https://p46-backend.herokuapp.com/estudiante');
      const data = await peticion.json();
      console.log(data);
      commit('setEstudiantes', data);
    },
    async registrarEstudiante({commit}, objEstudiante){
      //POST
      const peticion = await fetch('https://p46-backend.herokuapp.com/estudiante', {
        method: 'POST',
        headers:{
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(objEstudiante)
      });
      const data = await peticion.json();
      commit('pushEstudiante', data);
      console.log(data);
    },
    async cargarMunicipios({commit}){
      const peticion = await fetch('https://www.datos.gov.co/resource/xdk5-pm3f.json');
      const data = await peticion.json();
      console.log(data);
      commit('setMunicipios', data);
    },
    async eliminarEstudiante({commit}, obj){
      const peticion = await fetch('https://p46-backend.herokuapp.com/estudiante', {
        method: 'DELETE',
        headers:{
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
      });
    }
  },
  modules: {
  }
})



